# GameVim
GameVim is a total vim remapping that brings updates the key layout from 1976 to 2022!
In total there are less then 30 keybinds while maintaing all the functionality that vim is loved for.
This designed is inspired from the common WASD layout in video games which makes using and
learning vim *a lot* easier!

## The layout
Inside the `02_GameVim.vim` file there are all the keybindings with comments that explain them,
it is also the file where you can add/remove/modify any keymap you want.

The origianal vim keybinds have been deleted, and these are the new keybinds

| 2nd Modifier | Modifier | Key | Effect | Effect with modifier | Effect with 2nd modifier |
| ---------------- | ---------- | ----- | ------- | ------------------------ | ------------------------------ |
||
|                      | Shift       | **W**    | **Go up a character** | Go up a caracter | |
|Ctrl                | Shift       | **A**    | **Go left a character** | Go left a word | Go to the end of the line|
|                     | Shift       | **S**    | **Go down a character** | Go down a character | |
|Ctrl                | Shift       | **D**    | **Go right a character** | Go right a word | Go to the beginning of the line|
||
|                     | Shift       | **Q**    | **Delete a word** | Delete a line | |
|                     | Shift       | **E**    | **Insert mode** | Paste | |
||
|                     | Shift       | **R**    | **Undo** | Redo | |
||
|                     | Shift       | **F**    | **Search function** | Copy line | |
||
|                     |               | **X**    | **Delete a character** | | |
|                     |               | **BS** | **Delete a character** | | |
||
|Ctrl                |              |**Space** | **Visual block mode** | |Command Line mode |
||

## Will this break plugins?
No idea, I don't use plugins.

## How to install
Simply copy the files `01_GameVim_Unmap.vim` and `02_GameVim.vim` inside this folder :

`~/.config/nvim/after/plugins/`

Create the folder if it doesn't exists

## How to unistall?

Just delete these files

`~/.config/nvim/after/plugins/01_GameVim_Unmap.vim`

`~/.config/nvim/after/plugins/02_GameVim..vim`

## License
**CC BY-NC-SA 3.0**
