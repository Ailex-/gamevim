map ` <Nop>
map ~ <Nop>
map 1 <Nop>
map ! <Nop>
map 2 <Nop>
map @ <Nop>
map 3 <Nop>
map # <Nop>
map 4 <Nop>
map $ <Nop>
map 5 <Nop>
map % <Nop>
map 6 <Nop>
map ^ <Nop>
map 7 <Nop>
map & <Nop>
map 8 <Nop>
map * <Nop>
map 9 <Nop>
map ( <Nop>
map 0 <Nop>
map ) <Nop>
map - <Nop>
map _ <Nop>
map = <Nop>
map + <Nop>
map <BS><Nop>


map <Tab> <Nop>
map q <Nop>
map Q <Nop>
map w <Nop>
map W <Nop>
map e <Nop>
map E <Nop>
map r <Nop>
map R <Nop>
map t <Nop>
map T <Nop>
map y <Nop>
map Y <Nop>
map u <Nop>
map U <Nop>
map i <Nop>
map I <Nop>
map o <Nop>
map O <Nop>
map p <Nop>
map P <Nop>
map [ <Nop>
map { <Nop>
map ] <Nop>
map } <Nop>

"After some googling, apparently the Caps Lock key cannot be remapped inside vim"
map a <Nop>
map A <Nop>
map s <Nop>
map S <Nop>
map d <Nop>
map D <Nop>
map f <Nop>
map F <Nop>
map g <Nop>
map G <Nop>
map h <Nop>
map H <Nop>
map j <Nop>
map J <Nop>
map k <Nop>
map K <Nop>
map l <Nop>
map L <Nop>
map ; <Nop>
map : <Nop>
map ' <Nop>
map " <Nop>
map <Bslash> <Nop>
map <bar> <Nop>
map <Enter> <Nop>

map z <Nop>
map Z <Nop>
map x <Nop>
map X <Nop>
map c <Nop>
map C <Nop>
map v <Nop>
map V <Nop>
map b <Nop>
map B <Nop>
map n <Nop>
map N <Nop>
map m <Nop>
map M <Nop>
map , <Nop>
map < <Nop>
map . <Nop>
map > <Nop>
map / <Nop>
map ? <Nop>
map <Space> <Nop>


" The <Control + Key> keybindings "
map <C-wK> <Nop>
map <C-wJ> <Nop>
map <C-wL> <Nop>
map <C-wH> <Nop>
map <C-wk> <Nop>
map <C-wj> <Nop>
map <C-wl> <Nop>
map <C-wh> <Nop>
map <C-w=> <Nop>
map <C-wx> <Nop>
map <C-wq> <Nop>
map <C-ww> <Nop>
map <C-wv> <Nop>
map <C-ws> <Nop>
map <C-wT> <Nop>
map <C-w]> <Nop>
map <C-o> <Nop>
map <C-i> <Nop>
map <C-v> <Nop>
map <C-r> <Nop>
map <C-ox> <Nop>
map <C-rx> <Nop>
map <C-p> <Nop>
map <C-n> <Nop>
map <C-d> <Nop>
map <C-t> <Nop>
map <C-j> <Nop>
map <C-w> <Nop>
map <C-h> <Nop>
map <C-u> <Nop>
map <C-d> <Nop>
map <C-f> <Nop>
map <C-b> <Nop>
map <C-y> <Nop>
map <C-e> <Nop>
map <C-Space> <Nop>
map <C-q> <Nop>



" netrw, the vim file browser"
"unmap gx
"unmap <Plug>NetrwBrowseXVis
"unmap <Plug>NetrwBrowseX

" Some werid plugin I didn't ask to be installed, so I'm removing its mappings "
unmap g%
unmap ]%
unmap [%
unmap a%
unmap <Plug>(MatchitVisualTextObject)
unmap <Plug>(MatchitNormalForward)
unmap <Plug>(MatchitNormalBackward)
unmap <Plug>(MatchitVisualForward)
unmap <Plug>(MatchitVisualBackward)
unmap <Plug>(MatchitOperationForward)
unmap <Plug>(MatchitOperationBackward)
unmap <Plug>(MatchitNormalMultiBackward)
unmap <Plug>(MatchitNormalMultiForward)
unmap <Plug>(MatchitVisualMultiBackward)
unmap <Plug>(MatchitVisualMultiForward)
unmap <Plug>(MatchitOperationMultiBackward)
unmap <Plug>(MatchitOperationMultiForward)
