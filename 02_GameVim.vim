"__________________________________________________________________"
" -- Visual block mode, [Space] activates it and deactivates it -- "
" -- then [E] allows you to write                               -- "
nnoremap <Space> <C-v>
xnoremap <Space> <Esc>
xnoremap e I
xnoremap f yy

"__________________________________________________________________"
" -- Command line mode bind                                     -- "
nnoremap <Enter> :

"__________________________________________________________________"
" -- Bind [F] to search function and [Shift] + [F] to copy line -- "
nnoremap l /
noremap f yy
noremap <S-f> yw
"__________________________________________________________________"
" -- Movment binds, [W,A,S,D] go character by character,        -- "
" -- [Shift] + [W,A,S,D] go word by word and then [Ctrl]        -- "
" -- + [A,D] go to the beginning/end of the line                -- "
noremap w k
noremap s j
noremap a h
noremap d l

noremap <S-w> k
noremap <S-s> j
noremap <S-d> w
noremap <S-a> b

nnoremap <C-d> $
nnoremap <C-a> 0

"__________________________________________________________________"
" -- Binds [E] to insert text and [Shift] + [E] to paste        -- "
nnoremap e i
nnoremap <S-e> p

"__________________________________________________________________"
" -- The time travel key, [R] is undo, [Shift] + [R] is redo    -- "
noremap r u
noremap <S-r> <C-r>

"__________________________________________________________________"
" -- Binds [BackSpace] and [X] to delete a character            -- "
noremap <BS> x
noremap x x

"__________________________________________________________________"
" -- Binds [Q] to delete a word and [Shift] + [Q] to delete     -- "
" -- a line                                                     -- "
noremap q dw
noremap <S-q> dd

"__________________________________________________________________"
" -- Binds [c] to open or close a fold                          -- "
nnoremap c za
